
const projectName = 'WangCz'
export default {
    set(key, value) {
    // if (value) {
        localStorage.setItem(`${projectName  }_${  key}`, JSON.stringify(value))
    },
    get(key, defaultValue = null) {
        try {
            return JSON.parse(localStorage.getItem(`${projectName  }_${  key}`)) || defaultValue
        } catch (err) {
            return defaultValue
        }
    },
    remove(key) {
        localStorage.removeItem(`${projectName  }_${  key}`)
    },
    clear() {
    // localStorage.clear()
        // 为了使 hadc-admin-ui 和 hadc-ui 能在同一个域下 独立使用， 清除缓存时，需要区分来
        Object.keys(localStorage).forEach(key => {
            if (key.indexOf(projectName) !== -1) {
                // console.log('clear LocalStorage: ', key);
                localStorage.removeItem(key)
            }
        })
    },
}
