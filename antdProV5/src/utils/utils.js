// 对象转纯数组
export const objectToArrayPure = (obj) => {
    const list = [];
    for (const key in obj) {
        if (obj.hasOwnProperty(key)) {
            list.push(obj[key]);
        }
    }
    return list;
}
