import React, { useRef, useState } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import ProTable from '@ant-design/pro-table';
import { Button, Avatar, Switch, message } from 'antd';
import { PlusOutlined, UserOutlined } from '@ant-design/icons';
import { getUserInfo, lockUser } from '@/services/ant-design-pro/api';
// import CreateUser from './component/CreateUser'
// import EditUser from './component/EditUser'
import CreateOrEditUser from './component/CreateOrEditUser'


const UserList = () => {
    // 表格的ref 便于自定义操作表格
  const actionRef = useRef();
  // const [isModalVisible, setIsModalVisible] = useState(false);
  const [isModalVisibleEdit, setIsModalVisibleEdit] = useState(false);
  const [editId, setEditId] = useState();

  // 获取用户信息
  const getData = async (params) => {
    const result = await getUserInfo(params);

    // 抽出来后的固定格式
    return {
      data: result.data,
      success: true,
      total: result.meta.pagination.total,
    };
  };

  // 修改锁定用户
  const handleLockUser = async (uid) => {
    const result = await lockUser(uid);
    // result.status_code ? message.error('修改失败') : message.success('修改成功')
    if (result.status_code) {
      message.error('修改失败');
    }
    message.success('修改成功');
  };

  // // 修改创建用户弹出显隐
  // const isShowCreateUser = (status) =>{
  //   setIsModalVisible(status)
  // }

  // 子组件控制 编辑 开关
  const isShowEidtUser = (status) => {
    setIsModalVisibleEdit(status)
  }

  const columns = [
    {
      title: '头像',
      dataIndex: 'avatar_url',
      hideInSearch: true,
      render: (_, record) => <Avatar src={record.avatar_url} size={32} icon={<UserOutlined />} />,
    },
    {
      title: '姓名',
      dataIndex: 'name',
    },
    {
      title: '邮箱',
      dataIndex: 'email',
    },
    {
      title: '是否禁用',
      dataIndex: 'is_locked',
      hideInSearch: true,
      render: (_, record) => (
        <Switch
          checkedChildren="启动"
          unCheckedChildren="禁用"
          defaultChecked={record.is_locked === 0}
          onChange={() => {
            handleLockUser(record.id);
          }}
        />
      ),
    },
    {
      title: '创建时间',
      dataIndex: 'created_at',
      hideInSearch: true,
    },
    {
      title: '操作',
      hideInSearch: true,
      render: (_, record) => (
        <a onClick={() => {
            setEditId(record.id)
            isShowEidtUser(true);
          }} >
          编辑
        </a>
      ),
    },
  ];

  return (
    <PageContainer>
      <ProTable
        columns={columns}
        actionRef={actionRef}
        request={(params = {}) => getData(params)}
        rowKey="id"
        search={{
          labelWidth: 'auto',
        }}
        pagination={{ pageSize: 5 }}
        dateFormatter="string"
        headerTitle="用户列表"
        toolBarRender={() => [
          <Button
            key="button"
            icon={<PlusOutlined />}
            type="primary"
            onClick={() => {
              setEditId()
              isShowEidtUser(true);
            }}
          >
            新建
          </Button>,
        ]}
      />
      {
        !isModalVisibleEdit ? '' :
          <CreateOrEditUser isModalVisible={isModalVisibleEdit} isShowEidtUser={isShowEidtUser} actionRef={actionRef} editId={editId}></CreateOrEditUser>
      }

    </PageContainer>
  );
};

export default UserList;
