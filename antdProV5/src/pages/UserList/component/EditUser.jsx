import React, { useEffect, useState } from 'react';
import ProForm, { ProFormText } from '@ant-design/pro-form';
import { Modal, message, Skeleton } from 'antd';
import { editUserInfo, getDetailUserInfo } from '@/services/ant-design-pro/api';

const EditUser = (props) => {

  const { isModalVisible, editId } = props
  const { isShowEidtUser, actionRef } = props

  const [initialValues, setInitialValues] = useState()

  // 获取表单数据并提交
  const submitUserInfo = async (value) => {
    const result = await editUserInfo({ editId, value})
    if (result.status_code) {
      message.error('更新失败')
    }
    actionRef.current.reload();
    message.success('更新成功')
    isShowEidtUser(false)
  }


  const getUserInfoDetail = async () => {
    if (editId) {
      const userInfoDetail = await getDetailUserInfo(editId)
      setInitialValues(
        {
          name: userInfoDetail.name,
          email: userInfoDetail.email
        }
      )
    }
  }
  useEffect(() => {
    getUserInfoDetail();
  },[])


  return (
    <Modal
      title="编辑用户"
      visible={isModalVisible}
      onCancel={() => { isShowEidtUser(false) }}
      footer={null}
      destroyOnClose={true}
    >
      {
        initialValues ?
          <ProForm onFinish={(values) => submitUserInfo(values)} initialValues={initialValues}>
            <ProFormText
              name="name"
              label="名称"
              placeholder="请输入名称"
              rules={[{ required: true, message: '请输入名称' }]}
            />
            <ProFormText
              name="email"
              label="邮箱"
              placeholder="请输入邮箱"
              rules={[
                { required: true, message: '请输入邮箱' },
                { type: 'email', message: '输入正确格式邮箱' },
              ]}
            />
          </ProForm>
          :
          <Skeleton avatar paragraph={{ rows: 4 }} active={true}/>
      }

      {/* <ProFormText.Password
          name="password"
          label="密码"
          placeholder="请输入密码"
          rules={[
            { required: true, message: '请输入密码' },
            { min: 6, message: '请输入不小于6位的密码' },
          ]}
        /> */}

    </Modal>
  );
};

export default EditUser;
