import React from 'react';
import ProForm, { ProFormText } from '@ant-design/pro-form';
import {  Modal,message } from 'antd';
import { createUserInfo } from '@/services/ant-design-pro/api';

const CreateUser = (props) => {

  const { isModalVisible } = props
  const { isShowCreateUser,actionRef } = props

      // 获取表单数据并提交
  const submitUserInfo = async (value)=>{
    const result = await createUserInfo(value)
    if(result.status_code){
        message.error('创建失败')
    }
    actionRef.current.reload();
    message.success('创建成功')
    isShowCreateUser(false)
  }

  return (
    <Modal
      title="新建用户"
      visible={isModalVisible}
      onCancel={()=>{isShowCreateUser(false)}}
      footer={null}
      destroyOnClose={true}
    >
      <ProForm onFinish={(values) => submitUserInfo(values)}>
        <ProFormText
          name="name"
          label="名称"
          placeholder="请输入名称"
          rules={[{ required: true, message: '请输入名称' }]}
        />
        <ProFormText
          name="email"
          label="邮箱"
          placeholder="请输入邮箱"
          rules={[
            { required: true, message: '请输入邮箱' },
            { type: 'email', message: '输入正确格式邮箱' },
          ]}
        />
        <ProFormText.Password
          name="password"
          label="密码"
          placeholder="请输入密码"
          rules={[
            { required: true, message: '请输入密码' },
            { min: 6, message: '请输入不小于6位的密码' },
          ]}
        />
      </ProForm>
    </Modal>
  );
};

export default CreateUser;
