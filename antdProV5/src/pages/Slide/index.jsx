import React, { useRef, useState } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import ProTable from '@ant-design/pro-table';
import { Button, Image, Switch, message, Space, Popconfirm } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import { getSlideList, lockSlideList, deleteSlideInfo } from '@/services/ant-design-pro/slide';
// import CreateUser from './component/CreateUser'
// import EditUser from './component/EditUser'
import CreateOrEditUser from './component/CreateOrEditUser'


const UserList = () => {
    // 表格的ref 便于自定义操作表格
    const actionRef = useRef();
    // const [isModalVisible, setIsModalVisible] = useState(false);
    const [isModalVisibleEdit, setIsModalVisibleEdit] = useState(false);
    const [editId, setEditId] = useState();

    // 获取用户信息
    const getData = async (params) => {
        const result = await getSlideList(params);

        // 抽出来后的固定格式
        return {
            data: result.data,
            success: true,
            total: result.meta.pagination.total,
        };
    };

    // 修改锁定轮播图片
    const handleLockSlide = async (uid) => {
        const result = await lockSlideList(uid);
        if (result.status_code) {
            message.error('修改失败');
        }
        message.success('修改成功');
    };

    // // 修改创建用户弹出显隐
    // const isShowCreateUser = (status) =>{
    //   setIsModalVisible(status)
    // }

    // 子组件控制 编辑 开关
    const isShowEidtUser = (status) => {
        setIsModalVisibleEdit(status)
    }

    // 确定删除轮播图
    const deleteEditId = async (id) =>{
        if(id){
            const result = await deleteSlideInfo(id)
            message.error(result.status_code ? '删除轮播失败' : '已删除该轮播图')
            actionRef.current.reload();
        }
    }
    // 取消删除轮播图
    const cancelDeleteId = ()=>{
        message.error('已取消删除该轮播图');
    }

    const columns = [
        {
            title: '轮播图片',
            dataIndex: 'img_url',
            render: (_, record) => <Image src={record.img_url} width={80} />,
        },
        {
            title: '标题',
            dataIndex: 'title',
        },
        {
            title: '跳转链接',
            dataIndex: 'url',
        },
        {
            title: '是否禁用',
            dataIndex: 'is_locked',
            render: (_, record) => (
                <Switch
                    checkedChildren="启动"
                    unCheckedChildren="禁用"
                    defaultChecked={record.is_locked === 0}
                    onChange={() => {
                        handleLockSlide(record.id);
                    }}
                />
            ),
        },
        {
            title: '排序',
            dataIndex: 'seq',
            // editable: true,
        },
        {
            title: '更新时间',
            dataIndex: 'updated_at',
        },
        {
            title: '操作',
            hideInSearch: true,
            render: (_, record) => (
                <Space>
                    <a onClick={() => {
                        setEditId(record.id)
                        isShowEidtUser(true);
                    }} >
                        编辑
                    </a>
                    <Popconfirm
                        title="是否删除该轮播图?"
                        onConfirm={()=>deleteEditId(record.id)}
                        onCancel={() =>cancelDeleteId}
                        okText="确定"
                        cancelText="取消"
                    >
                        <a href="#">删除</a>
                    </Popconfirm>
                </Space>
            ),
        },
    ];

    return (
        <PageContainer>
            <ProTable
                columns={columns}
                actionRef={actionRef}
                request={(params = {}) => getData(params)}
                rowKey="id"
                search={false}
                pagination={{ pageSize: 5 }}
                dateFormatter="string"
                headerTitle="轮播管理"
                toolBarRender={() => [
                    <Button
                        key="button"
                        icon={<PlusOutlined />}
                        type="primary"
                        onClick={() => {
                            setEditId()
                            isShowEidtUser(true);
                        }}
                    >
                        新建
                    </Button>,
                ]}
            />
            {
                !isModalVisibleEdit ? '' :
                    <CreateOrEditUser isModalVisible={isModalVisibleEdit} isShowEidtUser={isShowEidtUser} actionRef={actionRef} editId={editId}></CreateOrEditUser>
            }

        </PageContainer>
    );
};

export default UserList;
