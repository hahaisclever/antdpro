import React, { useEffect, useState } from 'react';
import ProForm, { ProFormText } from '@ant-design/pro-form';
import { Modal, message, Button, Skeleton, Image } from 'antd';
import { getSlideDetail, editSlideInfo, createSlideInfo } from '@/services/ant-design-pro/slide'
import AliyunOssUpload from '@/components/AliyunOssUpload'
import { UploadOutlined } from '@ant-design/icons';

const EditUser = (props) => {

  const { isModalVisible, editId } = props
  const { isShowEidtUser, actionRef } = props
  const [formObj] = ProForm.useForm() // 得到ProForm的一个实例
  const [initialValues, setInitialValues] = useState(undefined)

  // const [options, setOptions] = useState()

  const title = editId ? '编辑商品' : '新建商品'

  // 文件上传成功后，子组件调用该方法传filekey到formObj中去通过表单校验
  const setCoverKey = (fileKey) => {
    formObj.setFieldsValue({ 'img': fileKey })
  }

  // 获取表单数据并提交
  const submitGoodsInfo = async (value) => {
    // 新增判断 id仅有一层的话 使用数组0的位置参数
    // const editValue = editId && value?.category_id.length > 1 ? { ...value, category_id: value.category_id[1] } : { ...value, category_id: value.category_id[0] }
    const editValue = value
    const result = editId ? await editSlideInfo({ editId, editValue }) : await createSlideInfo(editValue) // 解构成接口需要的参数类型
    if (result.status_code) {
      message.error('更新失败')
    }
    actionRef.current.reload();
    message.success('更新成功')
    isShowEidtUser(false)
  }

  // 当窗口用来编辑商品 用id去获取商品详情
  const getUserInfoDetail = async () => {
    if (editId !== undefined) {
      const response = await getSlideDetail(editId)
      setInitialValues(response)
    }
  }

  useEffect(() => {
    getUserInfoDetail()
  })


  return (
    <Modal
      title={title}
      visible={isModalVisible}
      onCancel={() => { isShowEidtUser(false) }}
      footer={null}
      destroyOnClose={true}
    >
      {
        initialValues === undefined && editId ?
          <Skeleton avatar paragraph={{ rows: 4 }} active={true} /> :
          <ProForm initialValues={initialValues} form={formObj} onFinish={(values) => submitGoodsInfo(values)}>
            <ProFormText
              name="title"
              label="轮播名"
              placeholder="请输入轮播名"
              rules={[
                { required: true, message: '请输入轮播名' },
              ]}
            />
            <ProFormText
              name="url"
              label="跳转的URL"
              placeholder="请输入跳转的URL地址"
              // rules={[
              //   { required: true, message: '请输入跳转的URL地址' },
              // ]}
            />
            <ProFormText
              name="seq"
              label="排序"
              placeholder="请输入轮播图的排序"
            />
            <ProFormText name="img" hidden={true}></ProFormText>
            <ProForm.Item name="img" label="轮播图" rules={[{ required: true, message: '请上传轮播图' }]}>
              <div>
                <AliyunOssUpload accept='image/*' setCoverKey={setCoverKey} showUploadList={true}>
                  {/* 里面的全部标签会传到组件的this.props.children */}
                  <Button icon={<UploadOutlined />}>请选择轮播图</Button>
                </AliyunOssUpload><p/>
                {
                  !initialValues?.img_url ? '' :
                    <Image width={200} src={initialValues?.img_url} ></Image>
                }
              </div>
            </ProForm.Item>
          </ProForm>

      }
    </Modal>
  );
};

export default EditUser;
