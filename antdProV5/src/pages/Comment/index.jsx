import React, { useEffect, useState } from 'react';
import { Statistic, Card, Row, Col } from 'antd';
import { ArrowUpOutlined, ArrowDownOutlined } from '@ant-design/icons';
import { getDashboardInfo } from '@/services/ant-design-pro/api'

const Comment = () => {
    const [dashboardInfo, setDashboardInfo] = useState({})

    const fetchData = async () => {
        const res = await getDashboardInfo({})
        setDashboardInfo(res)
    }

    useEffect(() => {
        fetchData()
    })
    return (
        <div className="site-statistic-demo-card">
            <Row gutter={16}>
                <Col span={8}>
                    <Card>
                        <Statistic
                            title="用户数量"
                            value={dashboardInfo.users_count}
                            precision={0}
                            valueStyle={{ color: '#3f8600' }}
                            prefix={<ArrowUpOutlined />}
                        />
                    </Card>
                </Col>
                <Col span={8}>
                    <Card>
                        <Statistic
                            title="商品数量"
                            value={dashboardInfo.goods_count}
                            precision={0}
                            valueStyle={{ color: '#cf1322' }}
                            prefix={<ArrowDownOutlined />}
                        />
                    </Card>
                </Col>
                <Col span={8}>
                    <Card>
                        <Statistic
                            title="订单数据"
                            value={dashboardInfo.order_count}
                            precision={0}
                            valueStyle={{ color: '#cf1322' }}
                            prefix={<ArrowDownOutlined />}
                        />
                    </Card>
                </Col>
            </Row>
        </div>
    )
}
export default Comment
