import React, { useRef, useState } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import ProTable from '@ant-design/pro-table';
import { Button, Image, Switch, message } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import { getGoodInfo, lockGoods, lockGoodsRecommend } from '@/services/ant-design-pro/api';
import CreateOrEditGoods from './component/CreateOrEditGoods'

const UserList = () => {
    // 表格的ref 便于自定义操作表格
    const actionRef = useRef();
    const [isModalVisibleEdit, setIsModalVisibleEdit] = useState(false);
    const [editId, setEditId] = useState();

    // 获取用户信息
    const getData = async (params) => {
        const result = await getGoodInfo(params);
        return {
            data: result.data,
            success: true,
            total: result.meta.pagination.total,
        };
    };

    // 是否上架
    const handleLockGoods = async (uid) => {
        const result = await lockGoods(uid);
        if (result.status_code) {
            message.error('上架失败');
        }
        message.success('上架成功');
    };

    // 是否推荐
    const handleLockGoodsRecommend = async (uid) => {
        const result = await lockGoodsRecommend(uid);
        if (result.status_code) {
            message.error('推荐失败');
        }
        message.success('推荐成功');
    };

    // 子组件控制 编辑 开关
    const isShowEidtUser = (status) => {
        setIsModalVisibleEdit(status)
    }

    const columns = [
        {
            title: '商品图片',
            dataIndex: 'cover_url',
            hideInSearch: true,
            render: (_, record) => <Image width={64} src={record.cover_url} placeholder={
                <Image
                    preview={false}
                    src={record.cover_url}
                    width={200}
                />
            }/>
        },
        {
            title: '商品名称',
            dataIndex: 'title',
            // hideInSearch: true,
        },
        {
            title: '价格',
            dataIndex: 'price',
            hideInSearch: true,
        },
        {
            title: '库存',
            dataIndex: 'stock',
            hideInSearch: true,
        },
        {
            title: '销量',
            dataIndex: 'sales',
            hideInSearch: true,
        },
        {
            title: '是否上架',
            dataIndex: 'is_on',
            // hideInSearch: true,
            render: (_, record) => (
                <Switch
                    checkedChildren="已上架"
                    unCheckedChildren="未上架"
                    defaultChecked={record.is_on === 1}
                    onChange={() => {
                        handleLockGoods(record.id);
                    }}
                />
            ),
            // 使用 valueType 和 valueEnum 得到搜索框的  valueEnum中0 1的key要根据record.is_on的值来改变
            valueType: 'radioButton',
            valueEnum:{
                1: { text: '已上架的' },
                0: { text: '未上架的' },
            }
        },
        {
            title: '是否推荐',
            dataIndex: 'is_recommend',
            // hideInSearch: true,
            render: (_, record) => (
                <Switch
                    checkedChildren="已推荐"
                    unCheckedChildren="未推荐"
                    defaultChecked={record.is_recommend === 1}
                    onChange={() => {
                        handleLockGoodsRecommend(record.id);
                    }}
                />
            ),
            valueType: 'radioButton',
            valueEnum: {
                1: { text: '已推荐的' },
                0: { text: '未推荐的' },
            }
        },
        {
            title: '创建时间',
            dataIndex: 'created_at',
            hideInSearch: true,
        },
        {
            title: '操作',
            hideInSearch: true,
            render: (_, record) => (
                <a onClick={() => {
                    setEditId(record.id)
                    isShowEidtUser(true);
                }} >
                    编辑
                </a>
            ),
        },
    ];

    return (
        <PageContainer>
            <ProTable
                columns={columns}
                actionRef={actionRef}
                request={(params = {}) => getData(params)}
                rowKey="id"
                search={{
                    labelWidth: 'auto',
                }}
                pagination={{ pageSize: 5 }}
                dateFormatter="string"
                headerTitle="用户列表"
                toolBarRender={() => [
                    <Button
                        key="button"
                        icon={<PlusOutlined />}
                        type="primary"
                        onClick={() => {
                            setEditId()
                            isShowEidtUser(true);
                        }}
                    >
                        新建
                    </Button>,
                ]}
            />
            {
                !isModalVisibleEdit ? '' :
                    <CreateOrEditGoods isModalVisible={isModalVisibleEdit} isShowEidtUser={isShowEidtUser} actionRef={actionRef} editId={editId}></CreateOrEditGoods>
            }

        </PageContainer>
    );
};

export default UserList;
