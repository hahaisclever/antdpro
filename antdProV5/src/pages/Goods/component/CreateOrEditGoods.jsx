import React, { useEffect, useState } from 'react';
import ProForm, { ProFormText, ProFormTextArea, ProFormDigit } from '@ant-design/pro-form';
import { Modal, message, Cascader, Button, Skeleton, Image } from 'antd';
import { editGoods, createGoods, getGoodsCategory, getGoodsDetail } from '@/services/ant-design-pro/api';
import AliyunOssUpload from '@/components/AliyunOssUpload'
import Editor from '@/components/Edit'
import { UploadOutlined } from '@ant-design/icons';

const EditUser = (props) => {

  const { isModalVisible, editId } = props
  const { isShowEidtUser, actionRef } = props
  const [formObj] = ProForm.useForm() // 得到ProForm的一个实例
  const [initialValues, setInitialValues] = useState(undefined)

  const [options, setOptions] = useState()

  const title = editId ? '编辑商品' : '新建商品'

  // 文件上传成功后，子组件调用该方法传filekey到formObj中去通过表单校验
  const setCoverKey = (fileKey) => {
    formObj.setFieldsValue({ 'cover': fileKey })
  }

    // 获得子组件 富文本 传来的值
  const setDetails = (content) =>{
    formObj.setFieldsValue({ 'details': content })
  }

  // 获取表单数据并提交
  const submitGoodsInfo = async (value) => {
    // 新增判断 id仅有一层的话 使用数组0的位置参数
    const editValue = editId && value?.category_id.length > 1 ? { ...value, category_id: value.category_id[1] } : { ...value, category_id: value.category_id[0] }
    const result = editId ? await editGoods({ editId, editValue }) : await createGoods({ ...value, category_id: value.category_id[1]}) // 解构成接口需要的参数类型
    if (result.status_code) {
      message.error('更新失败')
    }
    actionRef.current.reload();
    message.success('更新成功')
    isShowEidtUser(false)
  }

  const getGoodsCategoryList = async () => {
    const result = await getGoodsCategory(editId)
    if (result.status === undefined) setOptions(result)
  }

  // 当窗口用来编辑商品 用id去获取商品详情
  const getUserInfoDetail = async () => {
    if (editId !== undefined) {
      const response = await getGoodsDetail(editId)
      const { pid, id } = response.category
      console.log(response);
      setInitialValues({ ...response, category_id: pid !==0 ? [pid,id] : [id] }
      )
    }
  }

  useEffect(() => {
    getGoodsCategoryList()
    getUserInfoDetail()
  }, [])


  return (
    <Modal
      title={title}
      visible={isModalVisible}
      onCancel={() => { isShowEidtUser(false) }}
      footer={null}
      destroyOnClose={true}
    >
      {
      initialValues === undefined && editId ?
        <Skeleton avatar paragraph={{ rows: 4 }} active={true} /> :
        <ProForm initialValues={initialValues} form={formObj} onFinish={(values) => submitGoodsInfo(values)}>
          <ProForm.Item name="category_id" label="分类" rules={[{ required: true, message: '请输入名称' }]}>
            {/* fieldNames={{ label: 'name', value: 'id' }}  修改options默认的属性名称，使它与接口相同 */}
            <Cascader options={options} placeholder="请输入商品分类" fieldNames={{ label: 'name', value: 'id' }} />
          </ProForm.Item>
          <ProFormText
            name="title"
            label="商品名"
            placeholder="请输入商品名"
            rules={[
              { required: true, message: '请输入商品名' },
            ]}
          />
          <ProFormTextArea
            name="description"
            label="描述"
            placeholder="请输入商品描述"
            rules={[
              { required: true, message: '请输入商品描述' },
            ]}
          />
          <ProFormDigit
            name="price"
            label="价格"
            placeholder="请输入商品价格"
            min={0}
            max={99999}
            rules={[
              { required: true, message: '请输入商品价格' },
            ]}
          />
          <ProFormDigit
            name="stock"
            label="库存"
            placeholder="请输入商品库存"
            min={0}
            max={99999}
            rules={[
              { required: true, message: '请输入商品库存' },
            ]}
          />
          <ProFormText name="cover" hidden={true}></ProFormText>
          <ProForm.Item name="cover" label="商品主图" rules={[{ required: true, message: '请上传商品主图' }]}>
            <div>
              <AliyunOssUpload accept='image/*' setCoverKey={setCoverKey} showUploadList={true}>
                {/* 里面的全部标签会传到组件的this.props.children */}
                <Button icon={<UploadOutlined />}>上传商品图</Button>
              </AliyunOssUpload>
              {
                !initialValues?.cover_url ? '' :
                    <Image width={200} src={initialValues?.cover_url}></Image>
              }
            </div>
          </ProForm.Item>
          <ProForm.Item name="details" label="商品详情" rules={[{ required: true, message: '请输入商品详情' }]}>
            <Editor setDetails={setDetails} content={initialValues?.details}></Editor>
          </ProForm.Item>
        </ProForm>

      }
    </Modal>
  );
};

export default EditUser;
