import React, { useRef, useState } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import ProTable from '@ant-design/pro-table';
import { Button, message, Tag } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import { getOrderList } from '@/services/ant-design-pro/order'
import CreateOrEditUser from './component/CreateOrEditUser'


const UserList = () => {
    // 表格的ref 便于自定义操作表格
    const actionRef = useRef();
    // const [isModalVisible, setIsModalVisible] = useState(false);
    const [isModalVisibleEdit, setIsModalVisibleEdit] = useState(false);
    const [editId, setEditId] = useState();

    // 获取用户信息
    const getData = async (params) => {
        const result = await getOrderList(params);
        console.log(result);
        // 抽出来后的固定格式
        return {
            data: result.data,
            success: true,
            total: result.meta.pagination.total,
        };
    };

    // // 修改创建用户弹出显隐
    // const isShowCreateUser = (status) =>{
    //   setIsModalVisible(status)
    // }

    // 子组件控制 编辑 开关
    const isShowEidtUser = (status) => {
        setIsModalVisibleEdit(status)
    }

    const columns = [
        {
            title: '单号',
            dataIndex: 'order_no',
            // hideInSearch: true,
        },
        {
            title: '用户',
            dataIndex: 'user_id',
            hideInSearch: true,
        },
        {
            title: '金额',
            dataIndex: 'amount',
            hideInSearch: true,
        },
        {
            title: '状态',
            key: 'tags',
            hideInSearch: true,
            dataIndex: 'status',
            render: (_, record) => {
                let statueName = ''
                let color = ''
                switch (record.status) {
                    case 1:
                        statueName = '下单'
                        color = 'green'
                        break;
                    case 2:
                        statueName = '支付'
                        color = 'green'
                        break;
                    case 3:
                        statueName = '发货'
                        color = 'green'
                        break;
                    case 4:
                        statueName = '收货'
                        color = 'green'
                        break;
                    case 5:
                        statueName = '过期'
                        color = 'volcano'
                        break;
                    default:
                        break;
                }
                return  <Tag color={color}>
                    {statueName}
                </Tag>
            }
            // (
            //     <Tag color={record.status === 2 ? 'green' : 'volcano'}>
            //         {record.status}
            //     </Tag>
            // ),
        },
        {
            title: '支付类型',
            dataIndex: 'pay_type',
            hideInSearch: true,
        },
        {
            title: '支付单号',
            dataIndex: 'trade_no',
            // hideInSearch: true,
        },
        {
            title: '操作',
            hideInSearch: true,
            render: (_, record) => (
                <a onClick={() => {
                    setEditId(record.id)
                    isShowEidtUser(true);
                }} >
                    编辑
                </a>
            ),
        },
    ];

    return (
        <PageContainer>
            <ProTable
                columns={columns}
                actionRef={actionRef}
                request={(params = {}) => getData(params)}
                rowKey="id"
                search={{
                    labelWidth: 'auto',
                }}
                pagination={{ pageSize: 100 }}
                dateFormatter="string"
                headerTitle="订单管理"
                toolBarRender={() => [
                    <Button
                        key="button"
                        icon={<PlusOutlined />}
                        type="primary"
                        onClick={() => {
                            setEditId()
                            isShowEidtUser(true);
                        }}
                    >
                        新建
                    </Button>,
                ]}
            />
            {
                !isModalVisibleEdit ? '' :
                    <CreateOrEditUser isModalVisible={isModalVisibleEdit} isShowEidtUser={isShowEidtUser} actionRef={actionRef} editId={editId}></CreateOrEditUser>
            }

        </PageContainer>
    );
};

export default UserList;
