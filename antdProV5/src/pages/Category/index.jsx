import React, { useRef, useState } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import ProTable from '@ant-design/pro-table';
import { Button } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import { getCategoryList } from '@/services/ant-design-pro/categoryApi';
import CreateOrEditUser from './component/CreateOrEditUser'


const UserList = () => {
    // 表格的ref 便于自定义操作表格
    const actionRef = useRef();
    // const [isModalVisible, setIsModalVisible] = useState(false);
    const [isModalVisibleEdit, setIsModalVisibleEdit] = useState(false);
    const [editId, setEditId] = useState();

    // 获取分类信息
    const getData = async (params) => {
        const result = await getCategoryList(params);
        // // 抽出来后的固定格式
        return {
            data: result,
            success: true,
            // total: result.meta.pagination.total,
        };
    };

    // 子组件控制 编辑 开关
    const isShowEidtUser = (status) => {
        setIsModalVisibleEdit(status)
    }

    const columns = [
        {
            title: '分类名称',
            dataIndex: 'name',
            hideInSearch: true,
        },
        {
            title: '操作',
            hideInSearch: true,
            render: (_, record) => (
                record.pid !== 0 ?
                <a onClick={() => {
                    setEditId(record.id)
                    isShowEidtUser(true);
                }} >
                    编辑
                </a>:
                ''
            ),
        },
    ];

    return (
        <PageContainer>
            <ProTable
                columns={columns}
                actionRef={actionRef}
                request={(params = {}) => getData(params)}
                rowKey="id"
                search={false}
                // pagination={{ pageSize: 5 }}
                dateFormatter="string"
                headerTitle="分类名称"
                toolBarRender={() => [
                    <Button
                        key="button"
                        icon={<PlusOutlined />}
                        type="primary"
                        onClick={() => {
                            setEditId()
                            isShowEidtUser(true);
                        }}
                    >
                        新建
                    </Button>,
                ]}
            />
            {
                !isModalVisibleEdit ? '' :
                    <CreateOrEditUser isModalVisible={isModalVisibleEdit} isShowEidtUser={isShowEidtUser} actionRef={actionRef} editId={editId}></CreateOrEditUser>
            }

        </PageContainer>
    );
};

export default UserList;
