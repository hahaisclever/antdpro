import React, { useEffect, useState } from 'react';
import ProForm, { ProFormText } from '@ant-design/pro-form';
import { Modal, message, Skeleton, Select  } from 'antd';
import { getCategoryList, createCategory, getCategoryDetail, editCategory } from '@/services/ant-design-pro/categoryApi';

const { Option } = Select;

const EditUser = (props) => {

  const { isModalVisible, editId } = props
  const { isShowEidtUser, actionRef } = props
  const [formObj] = ProForm.useForm() // 得到ProForm的一个实例
  const [initialValues, setInitialValues] = useState(undefined)

  const [options, setOptions] = useState()

  const title = editId ? '编辑分类' : '新建分类'

  // 获取表单数据并提交
  const submitGoodsInfo = async (value) => {
    const result = editId ? await editCategory({ editId, value }) : await createCategory(value) // 解构成接口需要的参数类型
    if (result.status_code) {
      message.error('更新失败')
    }
    actionRef.current.reload();
    message.success('更新成功')
    isShowEidtUser(false)
  }

  // 获取下拉框数据
  const getGoodsCategoryList = async () => {
    const result = await getCategoryList()
    if (result.status === undefined) setOptions(result)
  }

  // 当窗口用来编辑分类 用id去获取分类详情
  const getUserInfoDetail = async () => {
    if (editId !== undefined) {
      const response = await getCategoryDetail(editId)
      setInitialValues(response)
      // console.log(123);
    }
  }

  useEffect(() => {
    getGoodsCategoryList()
    getUserInfoDetail()
  },[])


  return (
    <Modal
      title={title}
      visible={isModalVisible}
      onCancel={() => { isShowEidtUser(false) }}
      footer={null}
      destroyOnClose={true}
    >
      {
        initialValues === undefined && editId ?
          <Skeleton avatar paragraph={{ rows: 4 }} active={true} /> :
          <ProForm initialValues={initialValues} form={formObj} onFinish={(values) => submitGoodsInfo(values)}>
            <ProForm.Item name="pid" label="父分类" rules={[{ required: true, message: '请输入名称' }]}>
              <Select>
                {
                  options?options.map((item) => <Option key={item.id} value={item.id}>{item.name}</Option> ):''
                }
              </Select>
            </ProForm.Item>
            <ProFormText
              name="name"
              label="分类名称"
              placeholder="请输入分类名称"
              rules={[
                { required: true, message: '请输入分类名称' },
              ]}
            />
          </ProForm>

      }
    </Modal>
  );
};

export default EditUser;
