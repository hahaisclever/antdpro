/* eslint-disable */
import { request } from 'umi';
import local from '@/utils/localstorage'

/**获取分类列表接口 GET /api/admin/category */
export async function getCategoryList(body, options) {
  const token = local.get('token') || ''

  return request('/admin/category', {
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${token}`,
    },
    data: body,
    ...(options || {}),
  });
}

/**增加分类接口 POST /api/admin/category */
export async function createCategory(body, options) {
  const token = local.get('token') || ''
  return request('/admin/category', {
    params: body,
    method: 'POST',
    headers: {
      'Authorization': `Bearer ${token}`,
    },
    data: body,
    ...(options || {}),
  });
}

/**获取分类详情接口 GET /api/admin/category/{category} */
export async function getCategoryDetail(body, options) {
  const token = local.get('token') || ''
  const category = body
  return request(`/admin/category/${category}`, {
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${token}`,
    },
    data: body,
    ...(options || {}),
  });
}

/**修改分类接口 PUT /api/admin/category/{category} */
export async function editCategory(body, options) {
  const token = local.get('token') || ''
  const category = body.editId
  return request(`/admin/category/${category}`, {
    params: body.value,
    method: 'PUT',
    headers: {
      'Authorization': `Bearer ${token}`,
    },
    data: body,
    ...(options || {}),
  });
}
