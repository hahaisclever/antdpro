/* eslint-disable */
import { request } from 'umi';
import local from '@/utils/localstorage'

/**获取订单接口 GET /api/admin/orders */
export async function getOrderList(body, options) {
  const token = local.get('token') || ''

  return request('/admin/orders?include=goods,user，orderDetails', {
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${token}`,
    },
    data: body,
    ...(options || {}),
  });
}
