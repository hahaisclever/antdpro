/* eslint-disable */
import { request } from 'umi';
import local from '@/utils/localstorage'

/**获取轮播图片接口 GET /api/admin/slides */
export async function getSlideList(body, options) {
  const token = local.get('token') || ''

  return request('/admin/slides', {
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${token}`,
    },
    data: body,
    ...(options || {}),
  });
}

/**锁定轮播图片接口 PATCH /api/admin/slides/{slide}/status */
export async function lockSlideList(body, options) {
  const token = local.get('token') || ''
  const slide = body
  return request(`/admin/slides/${slide}/status`, {
    method: 'PATCH',
    headers: {
      'Authorization': `Bearer ${token}`,
    },
    data: body,
    ...(options || {}),
  });
}

/**获取轮播图片详情接口 GET /api/admin/slides/{slide} */
export async function getSlideDetail(body, options) {
  const token = local.get('token') || ''
  const slide = body
  return request(`/admin/slides/${slide}`, {
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${token}`,
    },
    data: body,
    ...(options || {}),
  });
}

/**修改轮播图片接口 PUT /api/admin/slides/{slide} */
export async function editSlideInfo(body, options) {
  const token = local.get('token') || ''
  return request(`/admin/slides/${body.editId}`, {
    params: body.editValue,
    method: 'PUT',
    headers: {
      'Authorization': `Bearer ${token}`,
    },
    data: body,
    ...(options || {}),
  });
}


/**创建轮播图片接口 POST /api/admin/slides */
export async function createSlideInfo(body, options) {
  const token = local.get('token') || ''
  return request(`/admin/slides`, {
    params: body,
    method: 'POST',
    headers: {
      'Authorization': `Bearer ${token}`,
    },
    data: body,
    ...(options || {}),
  });
}

/**删除轮播图片接口 DELETE /api/admin/slides/{slide} */
export async function deleteSlideInfo(body, options) {
  const token = local.get('token') || ''
  const slide = body
  return request(`/admin/slides/${slide}`, {
    method: 'DELETE',
    headers: {
      'Authorization': `Bearer ${token}`,
    },
    data: body,
    ...(options || {}),
  });
}
