// @ts-ignore
import local from '@/utils/localstorage'

/* eslint-disable */
import { request } from 'umi';
/** 获取当前的用户 GET /api/currentUser */

export async function currentUser(options) {
  const token = local.get('token') || ''
  return request('/admin/user', {
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${token}`,
    },
    ...(options || {}),
  });
}
/** 退出登录接口 POST /api/login/outLogin */

export async function outLogin(options) {
  const token = local.get('token') || ''
  return request('/auth/logout', {
    method: 'POST',
    headers: {
      'Authorization': `Bearer ${token}`,
    },
    ...(options || {}),
  });
}
/** 登录接口 POST /api/login/account */

export async function login(body, options) {
  const token = local.get('token') || ''

  return request('/auth/login', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`,
    },
    data: body,
    ...(options || {}),
  });
}
/** 此处后端没有提供注释 GET /api/notices */


/**  --------------------------------------------------------------   */

/**获取首页统计接口 GET /api/admin/index */
export async function getDashboardInfo(body, options) {
  const token = local.get('token') || ''

  return request('/admin/index', {
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${token}`,
    },
    data: body,
    ...(options || {}),
  });
}

/**获取用户信息接口 GET /api/admin/users */
export async function getUserInfo(body, options) {
  const token = local.get('token') || ''
  return request('/admin/users', {
    params:body,
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${token}`,
    },
    // data: body,
    ...(options || {}),
  });
}

/**修改用户信息接口 PATCH /api/admin/users/{user}/lock    */
export async function lockUser(body, options) {
  const token = local.get('token') || ''
  const uid = body
  return request(`/admin/users/${uid}/lock`, {
    method: 'PATCH',
    headers: {
      'Authorization': `Bearer ${token}`,
    },
    ...(options || {}),
  });
}

/**创建新的用户接口 POST /api/admin/users    */
export async function createUserInfo(body, options) {
  const token = local.get('token') || ''
  return request(`/admin/users`, {
    params: body,
    method: 'POST',
    headers: {
      'Authorization': `Bearer ${token}`,
    },
    ...(options || {}),
  });
}

/**修改用户信息接口 PUT /api/admin/users/{users}    */
export async function editUserInfo(body, options) {
  const token = local.get('token') || ''
  return request(`/admin/users/${body.editId}`, {
    params: body.value,
    method: 'PUT',
    headers: {
      'Authorization': `Bearer ${token}`,
    },
    ...(options || {}),
  });
}

/**获取用户信息详情接口 GET /api/admin/users/{user}    */
export async function getDetailUserInfo(body, options) {
  const token = local.get('token') || ''
  return request(`/admin/users/${body}`, {
    params: body,
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${token}`,
    },
    ...(options || {}),
  });
}

/**获取商品信息接口 GET /api/admin/goods */
export async function getGoodInfo(body, options) {
  const token = local.get('token') || ''
  return request('/admin/goods', {
    params:body,
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${token}`,
    },
    // data: body,
    ...(options || {}),
  });
}

/**商品上架和下架接口 PATCH /api/admin/goods/{good}/on    */
export async function lockGoods(body, options) {
  const token = local.get('token') || ''
  const good = body
  return request(`/admin/goods/${good}/on`, {
    method: 'PATCH',
    headers: {
      'Authorization': `Bearer ${token}`,
    },
    ...(options || {}),
  });
}

/**商品推荐和不推荐接口 PATCH /api/admin/goods/{good}/recommend    */
export async function lockGoodsRecommend(body, options) {
  const token = local.get('token') || ''
  const good = body
  return request(`/admin/goods/${good}/recommend`, {
    method: 'PATCH',
    headers: {
      'Authorization': `Bearer ${token}`,
    },
    ...(options || {}),
  });
}

/**商品分类列表 GET /api/admin/category    */
export async function getGoodsCategory(body, options) {
  const token = local.get('token') || ''
  return request(`/admin/category`, {
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${token}`,
    },
    ...(options || {}),
  });
}

/**从服务端获取OSS认证 GET /api/auth/oss/token    */
export async function ossConfig(body, options) {
  const token = local.get('token') || ''
  return request(`/auth/oss/token`, {
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${token}`,
    },
    ...(options || {}),
  });
}

/**商品上传接口 POST /api/admin/goods    */
export async function createGoods(body, options) {
  const token = local.get('token') || ''
  return request(`/admin/goods`, {
    params:body,
    method: 'POST',
    headers: {
      'Authorization': `Bearer ${token}`,
    },
    ...(options || {}),
  });
}

/**商品详情接口 GET /api/admin/goods/{good}    */
export async function getGoodsDetail(body, options) {
  const token = local.get('token') || ''
  const good = body
  return request(`/admin/goods/${good}?include=category`, {
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${token}`,
    },
    ...(options || {}),
  });
}

/**商品信息修改接口 PUT /api/admin/goods/{good}    */
export async function editGoods(data, options) {
  const good = data.editId
  const body = data.editValue
  const token = local.get('token') || ''
  return request(`/admin/goods/${good}`, {
    params:body,
    method: 'PUT',
    headers: {
      'Authorization': `Bearer ${token}`,
    },
    ...(options || {}),
  });
}


/**  --------------------------------------------------------------   */

export async function getNotices(options) {
  return request('/api/notices', {
    method: 'GET',
    ...(options || {}),
  });
}
/** 获取规则列表 GET /api/rule */

export async function rule(params, options) {
  return request('/api/rule', {
    method: 'GET',
    params: { ...params },
    ...(options || {}),
  });
}
/** 新建规则 PUT /api/rule */

export async function updateRule(options) {
  return request('/api/rule', {
    method: 'PUT',
    ...(options || {}),
  });
}
/** 新建规则 POST /api/rule */

export async function addRule(options) {
  return request('/api/rule', {
    method: 'POST',
    ...(options || {}),
  });
}
/** 删除规则 DELETE /api/rule */

export async function removeRule(options) {
  return request('/api/rule', {
    method: 'DELETE',
    ...(options || {}),
  });
}
