import React from 'react';
// 引入编辑器组件
import BraftEditor from 'braft-editor';
// 引入编辑器样式
import 'braft-editor/dist/index.css';
import './index.less';
import AliyunOssUpload from '@/components/AliyunOssUpload';
import { ContentUtils } from 'braft-utils';
// import { UploadOutlined } from '@ant-design/icons';
// import { Button,Icon } from 'antd';

export default class EditorDemo extends React.Component {
  state = {
    // 创建一个空的editorState作为初始值  简写的三元运算符  this.props.content?this.props.content:null
    editorState: BraftEditor.createEditorState(this.props.content ?? null),
  };

  // async componentDidMount() {
  //     // 假设此处从服务端获取html格式的编辑器内容
  //     const htmlContent = await fetchEditorContent()
  //     // 使用BraftEditor.createEditorState将html字符串转换为编辑器需要的editorStat
  //     this.setState({
  //         editorState: BraftEditor.createEditorState(htmlContent)
  //     })
  // }

  // submitContent = async () => {
  //     // 在编辑器获得焦点时按下ctrl+s会执行此方法
  //     // 编辑器内容提交到服务端之前，可直接调用editorState.toHTML()来获取HTML格式的内容
  //     const htmlContent = this.state.editorState.toHTML()
  //     const result = await saveEditorContent(htmlContent)
  // }

  handleEditorChange = (editorState) => {
    this.setState({ editorState });
    // 判断输入的DOM标签是否为空
    if (!editorState.isEmpty()) {
      // 调用父组件的方法，传入富文本的值 到form中
      const content = editorState.toHTML();
      this.props.setDetails(content);
    } else {
      this.props.setDetails('');
    }
  };

  insertImage = (url) => {
    this.setState({
      editorState: ContentUtils.insertMedias(this.state.editorState, [
        { type: 'IMAGE', url},
      ]),
    });
  };

  render() {
    // 自定义 富文本上传控件
    const extendControls = [
      {
        key: 'antd-uploader',
        type: 'component',
        component: (
          <AliyunOssUpload accept="image/*" showUploadList={false} insertImage={this.insertImage}>
            {/* 里面的全部标签会传到组件的this.props.children */}
            <button
              type="button"
              className="control-item button upload-button"
              data-title="插入图片"
            >
              插入图片
            </button>
          </AliyunOssUpload>
        ),
      },
    ];
    const { editorState } = this.state;
    return (
      <div className="my-editor">
        <BraftEditor
          value={editorState}
          onChange={this.handleEditorChange}
          extendControls={extendControls}
        />
      </div>
    );
  }
}
