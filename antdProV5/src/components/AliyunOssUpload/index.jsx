import React from 'react'
import { Upload, message } from 'antd';
import { ossConfig } from '@/services/ant-design-pro/api';

export default class AliyunOSSUpload extends React.Component {
    state = {
        OSSData: {},
    };

    // 组件挂载后，进行初始化，获取OSS上传前面
    async componentDidMount() {
        await this.init();
    }

    // 初始化 从服务端获取OSS
    init = async () => {
        try {
            const OSSData = await ossConfig();

            this.setState({
                OSSData,
            });
        } catch (error) {
            message.error(error);
        }
    };

    // 文件上传中触发的回调函数，直到上传完成
    onChange = ({ file }) => {
        if (file.status === 'done') {
            const {setCoverKey,insertImage} = this.props
            // 上传成功后，把表单formItem中的name填上，保障require可以通过
            if(setCoverKey){
                setCoverKey(file.key)
            }

            // 上次完成后，如果需要图片的url就返回图片的url到父组件中
            if(insertImage){
                insertImage(file.url)
            }
            message.success('上传成功')
        }
    };

    // 一般用在取消文件上传 通知OSS不上传 暂时不用到
    // onRemove = file => {
    //     const { value, onChange } = this.props;

    //     const files = value.filter(v => v.url !== file.url);

    //     if (onChange) {
    //         onChange(files);
    //     }
    // };

    // 额外的上传参数
    getExtraData = file => {
        const { OSSData } = this.state;

        return {
            key: file.key,
            OSSAccessKeyId: OSSData.accessid,
            policy: OSSData.policy,
            Signature: OSSData.signature,
        };
    };

    // 选择文件后 上传文件前的回调函数
    beforeUpload = async file => {
        const { OSSData } = this.state;
        const expire = OSSData.expire * 1000;

        // 前面过期了就重新获取
        if (expire < Date.now()) {
            await this.init();
        }

        const dir = 'react/'  // 定义OSS接受目录

        // 存到OSS 制定 服务端的文件夹里面
        const suffix = file.name.slice(file.name.lastIndexOf('.'));
        const filename = Date.now() + suffix;
        file.key = OSSData.dir + dir + filename;// 用来显示,  在getExtraData 函数中会用到，在云存储中存储的文件的KEY
        file.url = OSSData.host + OSSData.dir + dir + filename; // 文件路径   上传完成后，用于显示内容

        return file;
    };

    render() {
        const { value, accept, showUploadList } = this.props;
        const props = {
            accept: accept || '',
            name: 'file',
            fileList: value,
            action: this.state.OSSData.host,
            onChange: this.onChange,
            // onRemove: this.onRemove,
            data: this.getExtraData,
            beforeUpload: this.beforeUpload,
            listType: 'picture',  // 上传成功样式
            maxCount: 1, // 上传最大数量为1
            showUploadList,
        };
        return (
            <Upload {...props} >
                {this.props.children}
            </Upload>
        );
    }
}

