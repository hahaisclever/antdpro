export default [
  {
    path: '/index',
    layout: false,
    routes: [
      {
        path: '/index',
        routes: [
          {
            name: 'login',
            path: '/index/login',
            component: './Login',
          },
        ],
      },
    ],
  },
  {
    path: '/welcome',
    name: 'dashboard',
    icon: 'DashboardOutlined',
    component: './Dashborad',
  },
  {
    path: '/category',
    name: 'category',
    icon: 'MenuOutlined',
    component: './Category',
  },
  {
    path: '/userList',
    name: 'userList',
    icon: 'UserOutlined',
    component: './UserList',
  },
  {
    path: '/goods',
    name: 'goods',
    icon: 'BarsOutlined',
    component: './Goods',
  },
  {
    path: '/order',
    name: 'order',
    icon: 'FormOutlined',
    component: './Order',
  },
  {
    path: '/comment',
    name: 'comment',
    icon: 'CommentOutlined',
    component: './Comment',
  },
  {
    path: '/slide',
    name: 'slide',
    icon: 'FileImageOutlined',
    component: './Slide',
  },
  {
    path: '/',
    redirect: '/',
  },
  {
    component: './404',
  },
];
